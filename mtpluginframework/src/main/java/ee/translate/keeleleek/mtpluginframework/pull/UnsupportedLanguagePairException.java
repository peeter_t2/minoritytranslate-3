package ee.translate.keeleleek.mtpluginframework.pull;

/**
 * Thrown when the language pair is not supported.
 */
public class UnsupportedLanguagePairException extends Exception {

	private static final long serialVersionUID = 4581062922695095288L;
	
	private String srcLangCode;
	private String dstLangCode;
	private String pluginName;
	
	
	// INIT
	public UnsupportedLanguagePairException(String srcLangCode, String dstLangCode, String pluginName) {
		super("Language pair " + srcLangCode + "->" + dstLangCode + " is not supported by the " + pluginName + " plugin");
		this.srcLangCode = srcLangCode;
		this.dstLangCode = dstLangCode;
		this.pluginName = pluginName;
	}

	
	// DETAILS
	public String getSrcLangCode() {
		return srcLangCode;
	}
	
	public String getDstLangCode() {
		return dstLangCode;
	}
	
	public String getPluginName() {
		return pluginName;
	}
	
	
}
