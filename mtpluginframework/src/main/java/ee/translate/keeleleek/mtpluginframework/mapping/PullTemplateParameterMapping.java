package ee.translate.keeleleek.mtpluginframework.mapping;

import java.util.ArrayList;

import ee.translate.keeleleek.mtpluginframework.chopup.TemplateParameterElement;

/**
 * Mapping for template parameters.
 */
public class PullTemplateParameterMapping implements PullMapping {

	private String templateName;
	private String parameterName;
	private String originalDestination = null;
	private ArrayList<TemplateParameterElement> elements = new ArrayList<>();
	
	
	// INIT
	public PullTemplateParameterMapping(String templateName, String parameterName) {
		this.templateName = templateName;
		this.parameterName = parameterName;
	}

	public void addTemplateParameter(TemplateParameterElement element) {
		elements.add(element);
		if (elements.size() == 1) originalDestination = element.getName();
	}

	
	// VALUES
	public String getTemplateName() {
		return templateName;
	}
	
	public String getParameterName() {
		return parameterName;
	}

	public int getElementCount() {
		return elements.size();
	}

	
	// IMPLEMENT (PULL MAPPING)
	public String getGroup() {
		return "Template:" + templateName;
	}
	
	public String getSource() {
		return parameterName;
	}
	
	public String getDestination() {
		if (elements.size() == 0) return null;
		return elements.get(0).getName();
	}

	public void setDestination(String destination) {
		for (TemplateParameterElement element : elements) {
			element.setName(destination);
		}
	}
	
	public boolean isOriginal() {
		String origDest = this.originalDestination;
		String curentDest = getDestination();
		
		if (origDest == curentDest) return true; // null or same
		if (origDest == null) return false; // one is null, other is not
		
		return origDest.equals(curentDest); // check equality
	}
	
	@Override
	public boolean isTouched() {
		if (elements.size() == 0) return false;
		return elements.get(0).isNameTouched();
	}
	
	
}
