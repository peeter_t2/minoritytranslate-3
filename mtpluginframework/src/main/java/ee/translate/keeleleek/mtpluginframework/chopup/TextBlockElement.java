package ee.translate.keeleleek.mtpluginframework.chopup;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextBlockElement extends ArticleElement {

	private String text = "";
	
	public TextBlockElement() {
		super(Type.TEXT_BLOCK);
	}
	
	@Override
	public void init(String text) {
		this.text = text;
	}
	
	@Override
	protected String asCode() {
		return text + '\n';
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}

	public String translateAdaptText()
	 {
		String result = asCode();
		Set<Entry<Integer, ArticleElement>> entries = elements.entrySet();
		for (Entry<Integer, ArticleElement> entry : entries) {
			
			if (!(entry.getValue() instanceof WikilinkElement)) continue;
			
			WikilinkElement wikilink = (WikilinkElement) entry.getValue();
			
			result = result.replaceFirst(Pattern.quote("" + BGN + entry.getKey() + END), "" + BGN + entry.getKey() + "" + SEP + wikilink.getText() + END);
		}
		return result;
	 }

	public void translateDedaptText(String text)
	 {
		// handle wikilinks
		HashMap<String, String> cleanup = new HashMap<>();
		
		Pattern p = Pattern.compile(Pattern.quote("" + BGN) + "(.*?)" + Pattern.quote("" + END));
		Matcher m = p.matcher(text);
		
		while (m.find()) {
			
			String all = m.group(1);
			
			int i = all.indexOf(SEP);
			if (i == -1) continue;
			
			Integer key = Integer.parseInt(all.substring(0, i));
			String value = all.substring(i + 1);
			
			ArticleElement element = elements.get(key);
			if (element == null) continue;
			
			if (element instanceof WikilinkElement) {
				
				cleanup.put("" + BGN + key + "" + SEP + value + END, "" + BGN + key + END);
				
				WikilinkElement wikilink = (WikilinkElement) element;
				wikilink.setText(value);
				
			}
			
		}
		
		// cleanup text
		Set<Entry<String, String>> entries = cleanup.entrySet();
		for (Entry<String, String> entry : entries) {
			text = text.replace(entry.getKey(), entry.getValue());
		}
		
		setText(text);
	}
	
	public boolean hasContent()
	 {
		String text = this.text;
		
		Pattern p = Pattern.compile(Pattern.quote("" + BGN) + "(.*?)" + Pattern.quote("" + END));
		Matcher m = p.matcher(text);
		
		text = m.replaceAll("").trim();

		return !text.isEmpty();
	 }
	
	
}
