package ee.translate.keeleleek.mtapplication.model.serialising;

import java.lang.reflect.Type;
import java.util.regex.Pattern;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class ReferenceSerializer implements JsonSerializer<Reference>, JsonDeserializer<Reference> {

    @Override
    public JsonElement serialize(Reference ref, Type type, JsonSerializationContext jsc)
     {
        JsonObject jo = new JsonObject();
        jo.addProperty("qid", ref.getQid());
        jo.addProperty("langCode", ref.getLangCode());
        return jo;
     }

	@Override
	public Reference deserialize(JsonElement je, Type t, JsonDeserializationContext jdc) throws JsonParseException
	 {
		Reference ref;
        JsonObject jo;

        if (je.isJsonObject()) {
        	
            jo = je.getAsJsonObject();
            ref = new Reference(jo.get("qid").getAsString(), jo.get("langCode").getAsString());
        
        } else {
        	
            String jso = je.getAsString();
            String[] split = jso.split(Pattern.quote("&"), 2);
            ref = new Reference(split[0], split[1]);
            
        }

        return ref;
	 }
	
}
