package ee.translate.keeleleek.mtapplication.view.pages;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.model.preferences.TemplateMappingPreferences;


public abstract class TemplateMappingPageMediator extends Mediator implements PreferencesPage<TemplateMappingPreferences> {

	public final static String NAME = "{6D7838A4-9309-4448-859A-6B543E853942}";
	
	
	// INIT
	public TemplateMappingPageMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}

	
}
