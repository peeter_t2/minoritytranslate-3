package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import ee.translate.keeleleek.mtapplication.view.dialogs.PullProgressDialogMediator;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;


public class PullProgressDialogMediatorFX extends PullProgressDialogMediator {

	private int total = 1;
	
	@FXML
	private ProgressBar pullProgress;
	@FXML
	private Label actionLabel;
	
	
	// INIT
	@Override
	public void onRegister()
	 {
		
	 }

	
	// INHERIT (PROGRESS)
	@Override
	public void setAction(String action) {
		actionLabel.setText(action);
	}
	
	@Override
	public void setCurrent(int current) {
		pullProgress.setProgress((double)current / total);
	}
	
	@Override
	public void setTotal(int total) {
		this.total = total;
	}
	
	
}
