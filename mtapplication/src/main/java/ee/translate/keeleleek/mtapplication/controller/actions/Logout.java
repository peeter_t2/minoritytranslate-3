package ee.translate.keeleleek.mtapplication.controller.actions;

import net.sourceforge.jwbf.core.actions.Post;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.ApiRequestBuilder;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class Logout extends MWAction {
	
	private HttpAction action;
	
	private boolean popped = false;
	
	
	public Logout() {
		action = getAction();
	}
	
	public HttpAction getAction()
	 {
		Post action = new ApiRequestBuilder()
			.action("logout")
			.postParam("format", "xml")
			.buildPost();
		return action;
	 }
	
	@Override
	public HttpAction getNextMessage() {
		popped = true;
		return action;
	}

	@Override
	public boolean hasMoreMessages() {
		return !popped;
	}

	@Override
	public String processReturningText(String response, HttpAction action)
	 {
		return super.processReturningText(response, action);
	 }
	
}
