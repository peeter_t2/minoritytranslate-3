package ee.translate.keeleleek.mtapplication.controller.actions;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;

import net.sourceforge.jwbf.core.actions.RequestBuilder;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class SearchAction extends MWAction {

	private HttpAction action;
	
	private String search;
	private int namespace;
	
	private ArrayList<String> results = new ArrayList<>();
	private ArrayList<String> descriptions = new ArrayList<>();
	
	
	public SearchAction(String search, int namespace) {
		this.search = search;
		this.namespace = namespace;
		action = getAction();
	}
	
	public HttpAction getAction()
	 {
		HttpAction action = new RequestBuilder(MediaWiki.URL_API)
		.param("action", "opensearch")
		.param("search", MediaWiki.urlEncode(search))
		.param("redirects", "resolve")
		.param("namespace", namespace)
		.param("format", "xml")
		.buildGet();
		
		return action;
	 }

	@Override
	public HttpAction getNextMessage() {
		HttpAction action = this.action;
		this.action = null;
		return action;
	}

	@Override
	public boolean hasMoreMessages() {
		return action != null;
	}

	@Override
	public String processReturningText(String response, HttpAction action)
	 {
		//System.out.println("RESPONSE=" + response);
		
		response = StringEscapeUtils.unescapeJava(response);

		Pattern pItem = Pattern.compile(Pattern.quote("<Item>") + "(.+?)" + Pattern.quote("</Item>"));
		Pattern pText = Pattern.compile("<Text(?:.+?)>"+ "(.*?)" + "</Text>");
		Pattern pDescription = Pattern.compile("<Description(?:.+?)>"+ "(.*?)" + "</Description>");
		
		Matcher mItems = pItem.matcher(response);
		while (mItems.find()) {
			
			String item = mItems.group(1);
			
			Matcher mText = pText.matcher(item);
			if (mText.find()) {
				
				results.add(MediaWiki.urlDecode(mText.group(1)));
				
				Matcher mDescription = pDescription.matcher(item);
				if (mDescription.find()) descriptions.add(MediaWiki.urlDecode(mDescription.group(1)));
				else descriptions.add("");
				
			}
			
		}
		
		return super.processReturningText(response, action);
	 }

	/**
	 * Gets the search results.
	 * 
	 * @return search results
	 */
	public String[] getResults() {
		return results.toArray(new String[this.results.size()]);
	}
	
	public String[][] getExtendedResults()
	 {
		String[][] results = new String[this.results.size()][2];
		
		for (int i = 0; i < results.length; i++) {
			results[i][0] = this.results.get(i);
			results[i][1] = this.descriptions.get(i);
		}
		
		return results;
	 }
	
	
}
