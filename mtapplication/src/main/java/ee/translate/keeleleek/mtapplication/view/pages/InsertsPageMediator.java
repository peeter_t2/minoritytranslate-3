package ee.translate.keeleleek.mtapplication.view.pages;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.model.preferences.InsertsPreferences;


public abstract class InsertsPageMediator extends Mediator implements PreferencesPage<InsertsPreferences> {

	public final static String NAME = "{1AE545DE-FFF7-40B2-A8BB-0CDBAB5D8092}";
	
	
	// INIT
	public InsertsPageMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}

	
}
