package ee.translate.keeleleek.mtapplication.model.processing;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexCollect {

	private String rgxFind;
	private String[] params;

	
	// INIT
	public RegexCollect(String rgxFind, String param)
	 {
		this.rgxFind = rgxFind;
		this.params = new String[]{param};
	 }
	
	public RegexCollect(String find, String[] params)
	 {
		this.rgxFind = find;
		this.params = params;
	 }


	// COLLECT
	public void collect(String text, HashMap<String, String> paramMap)
	 {
		Matcher mFind = Pattern.compile(rgxFind, Pattern.MULTILINE | Pattern.DOTALL).matcher(text);
		
		// collecting parameter
		if (mFind.find()) {
			
			for (int i = 0; i < mFind.groupCount(); i++) {
				
				if (i >= params.length) break;
				
				paramMap.put(params[i], mFind.group(i + 1));
				
			}
		}
	 }
	
}
