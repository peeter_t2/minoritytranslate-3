package ee.translate.keeleleek.mtapplication.view.dialogs;

import org.puremvc.java.multicore.patterns.mediator.Mediator;


public abstract class UnicodeTableDialogMediator extends Mediator {

	public final static String NAME = "{FD11244F-E8D1-4FD8-9174-BE3AF0A09372}";
	
	
	// INIT
	public UnicodeTableDialogMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}
	
	
	// IMPLEMENT
	public abstract String getSymbols();
	
	
}
