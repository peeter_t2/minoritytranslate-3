package ee.translate.keeleleek.mtapplication.model.media;

import java.net.URL;

import javax.swing.ImageIcon;

public class LogosProxySwing extends LogosProxy {

	public final static String NAME = "{EE1EACEC-FFEA-4C1B-91DE-778E3CE133C9}";

	
	@Override
	public ImageIcon retrieveApplicationLogo() {
		return createImageIcon("logo.png");
	}

	@Override
	public ImageIcon retrieveKeeleleekLogo() {
		return createImageIcon("keeleleek.png");
	}

	@Override
	public ImageIcon retrieveWMFLogo() {
		return createImageIcon("WMF.png");
	}

	protected ImageIcon createImageIcon(String path) {
		URL imgURL = getClass().getResource("/" + path);
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

}
