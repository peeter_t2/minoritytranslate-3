package ee.translate.keeleleek.mtapplication.controller.wikis;

import java.io.IOException;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.model.content.WikiReference;
import ee.translate.keeleleek.mtapplication.model.serialising.ReferenceSerializer;
import ee.translate.keeleleek.mtapplication.model.serialising.WikiReferenceSerializer;
import ee.translate.keeleleek.mtapplication.model.wikis.WikisProxy;

public class SaveWikisCommand extends SimpleCommand {

	private static Logger LOGGER = LoggerFactory.getLogger(SaveWikisCommand.class);
	
	
	@Override
	public void execute(INotification notification)
	 {
		WikisProxy wikisProxy = MinorityTranslateModel.wikis();
		
		try {
			GsonBuilder builder = new GsonBuilder();
			builder.registerTypeAdapter(Reference.class, new ReferenceSerializer());
			builder.registerTypeAdapter(WikiReference.class, new WikiReferenceSerializer());
			builder.setPrettyPrinting();
			Gson gson = builder.create();
			String json = gson.toJson(wikisProxy);
			
			FileUtil.write(FetchWikisCommand.WIKIS_PATH, json);
		
		} catch (IOException e) {
			LOGGER.error("Failed to write wikis", e);
//			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.languages.save.failed"), Messages.getString("messages.languages.save.failed.to").replaceFirst("#", FetchLanguagesCommand.LANGUAGES_PATH.toString()).replaceFirst("#",e.getMessage()));
			return;
		}
	 }

	
}
