package ee.translate.keeleleek.mtapplication.model.gui;

import javax.swing.SwingUtilities;

public class GUINotifierProxySwing extends GUINotifierProxy {

	
	// INIT:
	public GUINotifierProxySwing() {
		super();
	}

	
	// NOTIFICATIONS:
	@Override
	public void sendNotificationThreadSafe(final String name, final Object body, final String type) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				sendNotification(name, body, type);
			}
		});
	}
	
	@Override
	public void sendNotificationThreadSafe(final String name, final Object body) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				sendNotification(name, body);
			}
		});
	}
	
	@Override
	public void sendNotificationThreadSafe(final String name) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				sendNotification(name);
			}
		});
	}
	
	@Override
	public void doThreadSafe(Runnable runnable) {
		SwingUtilities.invokeLater(runnable);
	}
	
}
