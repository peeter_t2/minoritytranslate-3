package ee.translate.keeleleek.mtapplication.model.lists;

public class CustomList implements SuggestionList {

	private String langCode;
	private String title;
	private String group;
	
	
	public CustomList(String langCode, String title, String group) {
		this.langCode = langCode;
		this.title = title;
		this.group = group;
	}
	
	
	public String getLangCode() {
		return langCode;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getName() {
		return title + " (" + langCode + ")";
	}

	public String getGroup() {
		return group;
	}
	
	
}
