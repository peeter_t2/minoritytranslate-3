package ee.translate.keeleleek.mtapplication.controller.actions;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import net.sourceforge.jwbf.core.actions.RequestBuilder;
import net.sourceforge.jwbf.core.actions.util.ActionException;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class AllLanguages extends MWAction {

	private HttpAction action;

	
	private boolean popped = false;
	
	
	private HashMap<String, String> languages = new HashMap<>();
	
	
	public AllLanguages() {
		action = getAction();
	}
	
	public HttpAction getAction()
	 {
		HttpAction action = new RequestBuilder(MediaWiki.URL_API)
		.param("action", "query")
		.param("meta", "siteinfo")
		.param("siprop", "languages")
		.param("format", "xml")
		.buildGet();
		return action;
	 }
	
	@Override
	public HttpAction getNextMessage() {
		popped = true;
		return action;
	}

	@Override
	public boolean hasMoreMessages() {
		return !popped;
	}

	@Override
	public String processReturningText(String response, HttpAction action)
	 {
		if (response.contains("error")) {
			Pattern errFinder = Pattern.compile("<p>(.*?)</p>", Pattern.DOTALL | Pattern.MULTILINE);
			Matcher m = errFinder.matcher(response);
			String lastP = "";
			while (m.find()) {
			  lastP = MediaWiki.urlDecode(m.group(1));
			}

			throw new ActionException("Language retrieval failed - " + lastP);
		}
		
		HashMap<String, String> result = new HashMap<>();
		
		Pattern pattern = Pattern.compile("<lang code=\"(.*?)</lang>");
		Matcher matcher = pattern.matcher(response);
		while (matcher.find()) {
			String element = matcher.group();
			
			String langCode = StringUtils.substringBetween(element, "<lang code=\"", "\"");
			String langName = StringUtils.substringBetween(element, "\">", "</lang>");
			
			if (langName.isEmpty()) continue;
			
			result.put(langCode, langName.toLowerCase());
		}
		
		languages = result;
		
		return super.processReturningText(response, action);
	 }

	/**
	 * Gets language all language codes and names.
	 * 
	 * @return language codes and names
	 */
	public HashMap<String, String> getLanguages() {
		return languages;
	}
	
}
