package ee.translate.keeleleek.mtapplication.controller.session;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.model.content.WikiReference;
import ee.translate.keeleleek.mtapplication.model.serialising.ReferenceSerializer;
import ee.translate.keeleleek.mtapplication.model.serialising.WikiReferenceSerializer;
import ee.translate.keeleleek.mtapplication.model.session.SessionProxy;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public class LoadSessionCommand extends SimpleCommand {

	public final static Charset ENCODING = StandardCharsets.UTF_8;
	
	
	private Logger LOGGER = LoggerFactory.getLogger(getClass());

	
	@Override
	public void execute(INotification notification)
	 {
		Path path = (Path) notification.getBody();
		
		// From file:
		String json;
		try {
			json = FileUtil.read(path);
		} catch (IOException e) {
			LOGGER.error("Failed to read session", e);
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.session.load.failed"), Messages.getString("messages.session.load.failed.to").replaceFirst("#", path.toString()).replaceFirst("#",e.getMessage()));
			return;
		}
		
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Reference.class, new ReferenceSerializer());
		builder.registerTypeAdapter(WikiReference.class, new WikiReferenceSerializer());
		Gson gson = builder.create();
		SessionProxy sessionProxy = gson.fromJson(json, SessionProxy.class);
		
		MinorityTranslateModel.lists().cancel();
		MinorityTranslateModel.lists().reset();
		MinorityTranslateModel.content().cancel();
		
		sessionProxy.setSessionPath(path);
		getFacade().registerProxy(sessionProxy);
	 }
	
}
