package ee.translate.keeleleek.mtapplication.model.filters;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReferencesFilter implements Filter {
	
	public final static String NAME = "references";
	
	
	@Override
	public String getName() {
		return NAME;
	}
	
	@Override
	public String filter(String text)
	 {
		while (true) {
	    	Pattern pattern = Pattern.compile(Pattern.quote("<ref") + "(.*?)" + Pattern.quote("</ref>"));
	    	Matcher matcher = pattern.matcher(text);
	    	if (!matcher.find()) break;
	    	text = text.substring(0, matcher.start()) + text.substring(matcher.end());
	    }
		
		while (true) {
	    	Pattern pattern = Pattern.compile(Pattern.quote("<ref") + "(.*?)" + Pattern.quote("/>"));
	    	Matcher matcher = pattern.matcher(text);
	    	if (!matcher.find()) break;
	    	text = text.substring(0, matcher.start()) + text.substring(matcher.end());
	    }
		return text;
	 }
	
}
