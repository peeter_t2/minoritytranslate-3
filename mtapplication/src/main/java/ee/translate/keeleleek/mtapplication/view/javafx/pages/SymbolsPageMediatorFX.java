package ee.translate.keeleleek.mtapplication.view.javafx.pages;

import java.util.ArrayList;
import java.util.List;

import ee.translate.keeleleek.mtapplication.model.preferences.Symbol;
import ee.translate.keeleleek.mtapplication.model.preferences.SymbolsPreferences;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.SymbolFX;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.pages.SymbolsPageMediator;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.util.Callback;


public class SymbolsPageMediatorFX extends SymbolsPageMediator {

	@FXML
	private TableView<SymbolFX> symbolsTable;
	@FXML
	private TableColumn<SymbolFX, String> filterColumn;
	@FXML
	private TableColumn<SymbolFX, String> descriptionColumn;
	@FXML
	private TableColumn<SymbolFX, String> triggerColumn;
	@FXML
	private TableColumn<SymbolFX, String> symbolColumn;
	
	@FXML
	private TextField srcLangEdit;
	@FXML
	private TextField dstLangEdit;
	@FXML
	private TextField descriptionEdit;
	@FXML
	private TextField triggerEdit;
	@FXML
	private TextField symbolEdit;

	@FXML
	private ComboBox<String> rowSelect;
	
	@FXML
	private Button removeButton;
	@FXML
	private Button moveUpButton;
	@FXML
	private Button moveDownButton;

	
	// INIT
	@FXML
	protected void initialize()
	 {
		rowSelect.getItems().add(Messages.getString("preferences.content.assist.symbols.row.none"));
		rowSelect.getItems().add(Messages.getString("preferences.content.assist.symbols.row.first"));
		rowSelect.getItems().add(Messages.getString("preferences.content.assist.symbols.row.second"));
		rowSelect.getItems().add(Messages.getString("preferences.content.assist.symbols.row.third"));
		
		filterColumn.prefWidthProperty().bind(symbolsTable.widthProperty().divide(6));
		descriptionColumn.prefWidthProperty().bind(symbolsTable.widthProperty().divide(2.45));
		triggerColumn.prefWidthProperty().bind(symbolsTable.widthProperty().divide(4));
		symbolColumn.prefWidthProperty().bind(symbolsTable.widthProperty().divide(6));
		
		filterColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SymbolFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<SymbolFX, String> symbol) {
				return symbol.getValue().dstLang;
			}
		});
		
		descriptionColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SymbolFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<SymbolFX, String> symbol) {
				return symbol.getValue().name;
			}
		});

		triggerColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SymbolFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<SymbolFX, String> symbol) {
				return symbol.getValue().trigger;
			}
		});

		symbolColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SymbolFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<SymbolFX, String> symbol) {
				return new SimpleStringProperty(symbol.getValue().symbol + "");
			}
		});
		
		symbolsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<SymbolFX>() {
			@Override
			public void changed(ObservableValue<? extends SymbolFX> obs, SymbolFX oldVal, SymbolFX newVal)
			 {
				if (oldVal != null) {
					oldVal.dstLang.unbind();
					oldVal.name.unbind();
					oldVal.trigger.unbind();
					oldVal.row.unbind();
					removeButton.setDisable(true);
					moveUpButton.setDisable(true);
					moveDownButton.setDisable(true);
				}
				if (newVal != null) {
					
					srcLangEdit.setText(newVal.dstLang.getValue());
					newVal.dstLang.bind(srcLangEdit.textProperty());
					
					descriptionEdit.setText(newVal.name.getValue());
					newVal.name.bind(descriptionEdit.textProperty());

					triggerEdit.setText(newVal.trigger.getValue());
					newVal.trigger.bind(triggerEdit.textProperty());
					
					rowSelect.getSelectionModel().select(newVal.row.getValue() + 1);
					newVal.row.bind(rowSelect.getSelectionModel().selectedIndexProperty().subtract(1));
					
					symbolEdit.setText(newVal.symbol + "");

					removeButton.setDisable(false);
					moveUpButton.setDisable(false);
					moveDownButton.setDisable(false);
				}
			 }
		});
	 }

	
	// SYMBOLS
	public void addSymbol(int i, char symbol)
	 {
		String desc = Character.getName(symbol);
		if (desc != null) desc = desc.toLowerCase().replace('_', ' ');
		SymbolFX symbolFX = new SymbolFX(new Symbol(desc, "", symbol));
		
		symbolsTable.getItems().add(i, symbolFX);
		
		symbolsTable.getSelectionModel().select(i);
	 }
	
	public void removeSymbol(int i)
	 {
		if (i == -1) return;
		
		symbolsTable.getItems().remove(i);
		
		symbolsTable.getSelectionModel().select(i);
	 }
	
	
	// INHERIT
	public void open(SymbolsPreferences preferences)
	 {
		List<Symbol> symbolList = preferences.getSymbols();
		ObservableList<SymbolFX> symbolListFX = FXCollections.observableArrayList();
		for (Symbol symbol : symbolList) {
			symbolListFX.add(new SymbolFX(symbol));
		}
		
		symbolsTable.setItems(symbolListFX);
		
		srcLangEdit.setText("");
		descriptionEdit.setText("");
		triggerEdit.setText("");
		symbolEdit.setText("");
		
		symbolsTable.getSelectionModel().selectFirst();
	 }
	

	@Override
	public SymbolsPreferences close()
	 {
		ObservableList<SymbolFX> symbolsFX = symbolsTable.getItems();
		ArrayList<Symbol> symbolsList = new ArrayList<>(symbolsFX.size());
		for (SymbolFX symbolFX : symbolsFX) {
			symbolsList.add(symbolFX.toSymbol());
		}
		
		return new SymbolsPreferences(symbolsList);
	 }
	

	@Override
	public void addSymbol(char symbol)
	 {
		addSymbol(symbolsTable.getItems().size(), symbol);
	 }
	
	
	// EVENTS
	@FXML
	private void onAddBeforeClick()
	 {
		int i = symbolsTable.getSelectionModel().getSelectedIndex();
		if (i == -1) i = 0;
		
		String symbols = askSymbols();
		if (symbols == null) return;
		for (int j = 0; j < symbols.length(); j++) {
			addSymbol(i + j, symbols.charAt(j));
		}
	 }
	
	@FXML
	private void onAddAfterClick()
	 {
		int i = symbolsTable.getSelectionModel().getSelectedIndex();
		if (i == -1) i = symbolsTable.getItems().size();
		else i++;
		
		String symbols = askSymbols();
		if (symbols == null) return;
		for (int j = 0; j < symbols.length(); j++) {
			addSymbol(i + j, symbols.charAt(j));
		}
	 }

	@FXML
	private void onRemoveClick()
	 {
		removeSymbol(symbolsTable.getSelectionModel().getSelectedIndex());
	 }

	@FXML
	private void onMoveUpClick()
	 {
		int i = symbolsTable.getSelectionModel().getSelectedIndex();
		if (i == -1 || i <= 0) return;

		SymbolFX symbolFX = symbolsTable.getItems().remove(i);
		
		i--;
		symbolsTable.getItems().add(i, symbolFX);
		symbolsTable.getSelectionModel().select(i);
	 }

	@FXML
	private void onMoveDownClick()
	 {
		int i = symbolsTable.getSelectionModel().getSelectedIndex();
		if (i == -1 || i >= symbolsTable.getItems().size() - 1) return;

		SymbolFX symbolFX = symbolsTable.getItems().remove(i);
		
		i++;
		symbolsTable.getItems().add(i, symbolFX);
		symbolsTable.getSelectionModel().select(i);
	 }

	@FXML
	private void onImportSetClick()
	 {
		super.onImportSymbolSet();
	 }

	@FXML
	private void onExportSetClick()
	 {
		ObservableList<SymbolFX> symbolsFX = symbolsTable.getItems();
		ArrayList<Symbol> symbolsList = new ArrayList<>(symbolsFX.size());
		for (SymbolFX symbolFX : symbolsFX) {
			symbolsList.add(symbolFX.toSymbol());
		}
		
		super.onExportSymbolSet(symbolsList);
	 }

	
}
