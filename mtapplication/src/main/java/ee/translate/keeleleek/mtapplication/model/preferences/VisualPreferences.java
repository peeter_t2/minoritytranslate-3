package ee.translate.keeleleek.mtapplication.model.preferences;

import ee.translate.keeleleek.mtapplication.view.views.ViewMediator.ViewMode;

public class VisualPreferences {

	private int addonsDivider;
	private double translateDivider;
	private ViewMode view;
	
	// INIT
	private VisualPreferences()
	 {

	 }

	public VisualPreferences(VisualPreferences other)
	 {
		this.translateDivider = other.translateDivider;
		this.addonsDivider = other.addonsDivider;
		this.view = other.view;
	 }
	
	
	public static VisualPreferences create()
	 {
		VisualPreferences preferences = new VisualPreferences();
		
		preferences.addonsDivider = 200;
		preferences.translateDivider = 0.5;
		preferences.view = ViewMode.SPLIT_HORIZONTAL;
		
		return preferences;
	 }
	

	// PREFERENCES
	public int getAddonsDivider() {
		return addonsDivider;
	}
	
	public void setAddonsDivider(int addonsDivider) {
		this.addonsDivider = addonsDivider;
	}
	
	public double getTranslateDivider() {
		return translateDivider;
	}
	
	public void setTranslateDivider(double translateDivider) {
		this.translateDivider = translateDivider;
	}
	
	public ViewMode getView() {
		return view;
	}
	
	public void setView(ViewMode view) {
		this.view = view;
	}
	

	// UTILITY
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + addonsDivider;
		long temp;
		temp = Double.doubleToLongBits(translateDivider);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((view == null) ? 0 : view.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VisualPreferences other = (VisualPreferences) obj;
		if (addonsDivider != other.addonsDivider)
			return false;
		if (Double.doubleToLongBits(translateDivider) != Double.doubleToLongBits(other.translateDivider))
			return false;
		if (view != other.view)
			return false;
		return true;
	}

	
}
