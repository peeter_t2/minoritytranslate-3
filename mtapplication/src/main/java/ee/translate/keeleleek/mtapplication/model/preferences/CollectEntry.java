package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.Arrays;

import ee.translate.keeleleek.mtapplication.model.processing.RegexCollect;

public class CollectEntry implements TableEntry {
	
	public final static String SEPARATOR = ";";
	public final static String REGEX_PREFIX = "REGEX:";
	final public static String COLLECT_WIKI_PUNCTUATION_REGEX = "[\\!\"#\\$%&'\\(\\)\\*\\+,\\-\\./:;\\<=\\>\\?@\\[\\\\\\]\\^_`\\{\\|\\}~]";
	
	private EntryFilter filter;
	private String find;
	private String[] params;
	
	
	// INIT
	public CollectEntry()
	 {
		this.filter = new EntryFilter();
		this.find = "";
		this.params = new String[0];
	 }

	public CollectEntry(EntryFilter filter, String find, String[] params)
	 {
		this.filter = filter;
		this.find = find;
		this.params = params;
	 }

	public CollectEntry(EntryFilter filter, String find, String params)
	 {
		this(filter, find, params.split(SEPARATOR));
	 }
	
	public CollectEntry(CollectEntry other) {
		this.filter = new EntryFilter(other.filter);
		this.find = other.find;
		this.params = new String[other.params.length];
		for (int i = 0; i < other.params.length; i++) this.params[i] = other.params[i];
	}
	
	
	// REGEX
	public RegexCollect createRegex()
	 {
		if (find.startsWith(REGEX_PREFIX)) return new RegexCollect(find.substring(REGEX_PREFIX.length()), params);
		String rgxFind = find;
		
		if (rgxFind.startsWith("_")) rgxFind = "(?<=\\s|\\p{Punct} )" + "\\Q" + rgxFind.substring(1, rgxFind.length());
		else if (rgxFind.startsWith("#")) rgxFind = "^" + "\\Q" + rgxFind;
		else rgxFind = "\\Q" + rgxFind; // treat as one (start)

		if (rgxFind.endsWith("_")) rgxFind = rgxFind.substring(0, rgxFind.length() - 1) + "\\E" + "(?=\\s|\\p{Punct} )";
		else if (rgxFind.endsWith("#")) rgxFind = rgxFind + "\\E" + "$";
		else rgxFind = rgxFind + "\\E"; // treat as one (end)
		
		rgxFind = rgxFind.replace("#", "\\E(.*?)\\Q"); // capture params
		
		rgxFind = rgxFind.replace("~", "\\E(?:.*?)\\Q"); // anything between
		
		rgxFind = rgxFind.replace("\\Q\\E", ""); // clean
		
		return new RegexCollect(rgxFind, params);
	 }

	
	// DATA
	public EntryFilter getFilter() {
		return filter;
	}

	public void setFilter(EntryFilter filter) {
		this.filter = filter;
	}

	public String getFind() {
		return find;
	}

	public void setFind(String find) {
		this.find = find;
	}

	public String[] getParams() {
		return params;
	}

	public void setParams(String[] params) {
		this.params = params;
	}

	
	// CONVERSION
	@Override
	public String toStringEntry(int c)
	 {
		switch (c) {
		case 0:
			String src = getFilter().getSrcLangRegex();
			String dst = getFilter().getDstLangRegex();
			return src + " ; " + dst;
			
		case 1:
			return getFind().replace("\n", "\\n");
			
		case 2:
			StringBuilder result = new StringBuilder();
			String[] parameters = getParams();
			for (int i = 0; i < parameters.length; i++) {
				if (i > 0) result.append(" ; ");
				result.append(parameters[i]);
			}
			return result.toString();

		default:
			return null;
		}
	 }
	
	@Override
	public void fromStringEntry(int c, String value)
	 {
		switch (c) {
		case 0:
			String[] split = value.split(";", 2);
			String src = split[0].trim();
			String dst = split.length == 2 ? split[1].trim() : null;
			setFilter(new EntryFilter(src, dst));
			break;
			
		case 1:
			setFind(value.replace("\\n", "\n"));
			break;
			
		case 2:
			String[] parameters = value.split(";");
			for (int i = 0; i < parameters.length; i++) {
				parameters[i] = parameters[i].trim();
			}
			setParams(parameters);
			break;

		default:
			break;
		}
	 }
	
	
	// HELPERS
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filter == null) ? 0 : filter.hashCode());
		result = prime * result + ((find == null) ? 0 : find.hashCode());
		result = prime * result + Arrays.hashCode(params);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		CollectEntry other = (CollectEntry) obj;
		if (filter == null) {
			if (other.filter != null) return false;
		} else if (!filter.equals(other.filter)) return false;
		if (find == null) {
			if (other.find != null) return false;
		} else if (!find.equals(other.find)) return false;
		if (!Arrays.equals(params, other.params)) return false;
		return true;
	}
	
	
}
