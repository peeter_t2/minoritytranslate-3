package ee.translate.keeleleek.mtapplication.model.processing;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.PatternSyntaxException;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.WikiType;
import ee.translate.keeleleek.mtapplication.model.content.Namespace;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.model.processing.processers.OldLanglinksProcesser;
import ee.translate.keeleleek.mtapplication.model.processing.processers.RegexCollectProcesser;
import ee.translate.keeleleek.mtapplication.model.processing.processers.RegexReplaceProcesser;

public class ProcesserProxy extends Proxy {

	public final static String NAME = "{FFB15C82-48F1-434C-989A-06DF5DE18D25}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(ProcesserProxy.class);

	private final static String TITLE_VARIABLE_LOWERCASE = "${title}";
	private final static String TITLE_VARIABLE_UPPERCASE = "${Title}";
	
	transient private OldLanglinksProcesser oldLanglinksProcesser = new OldLanglinksProcesser();
	transient private RegexReplaceProcesser regexReplaceProcesser = new RegexReplaceProcesser();
	transient private RegexCollectProcesser regexCollectProcesser = new RegexCollectProcesser();
	
	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public ProcesserProxy() {
		super(NAME, "");
	}

	
	
	/* ******************
	 *                  *
	 *    Processing    *
	 *                  *
	 ****************** */
	public String pullProcess(String srcLangCode, String dstLangCode, String srcTitle, String srcText, String dstTitle, String dstText) throws PatternSyntaxException
	 {
		//LOGGER.info("Pullprocessing " + srcTitle + " to " + dstTitle);

		WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(dstLangCode);
		List<RegexCollect> collects;
		ArrayList<RegexReplace> replaces;
		switch (wikiType) {
		case REGULAR:
		case INCUBATOR:
			collects = MinorityTranslateModel.preferences().processing().filterCollects(srcLangCode, dstLangCode);
			replaces = MinorityTranslateModel.preferences().processing().filterReplaces(srcLangCode, dstLangCode);
			return regexReplaceProcesser.process(srcTitle, srcText, dstTitle, dstText, collects, replaces);
		
		default:
			return "";
		}
	 }
	
	public String pasteProcess(String dstLangCode, String dstTitle, String dstText, String pasteText) throws PatternSyntaxException
	 {
		//LOGGER.info("Pasteprocessing from " + article);

		WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(dstLangCode);
		switch (wikiType) {
		case REGULAR:
		case INCUBATOR:
			return regexCollectProcesser.process(pasteText, dstTitle, dstText, MinorityTranslateModel.preferences().processing().filterCollects(dstLangCode, dstLangCode));
		
		default:
			return "";
		}
	 }
	
	public String variableProcess(String text, String langCode)
	 {
		String qid = MinorityTranslateModel.content().getSelectedQid();
		Reference ref = null;
		MinorityArticle article = null;
		if (qid != null) ref = new Reference(qid, langCode);
		if (ref != null) article = MinorityTranslateModel.content().getArticle(ref);
		
		text = namespaceProcess(text, langCode);
		if (article != null) text = titleProcess(text, article.getTitle());
		
		return text;
	 }
	
	public String namespaceProcess(String text, String langCode)
	 {
		Namespace[] namespaces = Namespace.values();
		for (Namespace namespace : namespaces) {
			
			String nsName = MinorityTranslateModel.wikis().findNamespace(langCode, namespace);
			if (nsName == null || nsName.isEmpty()) continue;
			
			text = text.replace(namespace.getVariableName(), nsName);
			
		}
		
		return text;
	 }
	
	public String titleProcess(String text, String title)
	 {
		if (title.isEmpty()) return text;

		String utitle = title.substring(0, 1).toUpperCase() + title.substring(1);
		String ltitle = title.substring(0, 1).toLowerCase() + title.substring(1);

		text = text.replace(TITLE_VARIABLE_LOWERCASE, ltitle);
		text = text.replace(TITLE_VARIABLE_UPPERCASE, utitle);
		
		return text;
	 }
	
	
	
	/* ******************
	 *                  *
	 *     Handling     *
	 *                  *
	 ****************** */
	public boolean handleProcess(MinorityArticle article)
	 {
		LOGGER.info("Postprocessing " + article);
		
		Reference ref = article.getRef();
		
		boolean processed = false;
		
		WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(ref.getLangCode());
		switch (wikiType) {
		case REGULAR:
			break;
		
		case INCUBATOR:
			processed = oldLanglinksProcesser.process(article) || processed;
			break;
		
		default:
			break;
		}
		
		if (processed) MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_PROCESSED, ref);
		
		return processed;
	  }
	
	
	// HELPERS
	public List<String> collectAllVariables() {
		ArrayList<String> variables = new ArrayList<>();

		variables.add(TITLE_VARIABLE_LOWERCASE);
		variables.add(TITLE_VARIABLE_UPPERCASE);
		
		Namespace[] namespaces = Namespace.values();
		for (Namespace namespace : namespaces) {
			if (namespace.isEmpty()) continue;
			variables.add(namespace.getVariableName());
		}
		
		return variables;
	}
	
	
}
