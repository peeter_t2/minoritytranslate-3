package ee.translate.keeleleek.mtapplication.controller.preferences;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class ChangePreferencesListsEnabledCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		Boolean enabled = (Boolean) notification.getBody();
		MinorityTranslateModel.preferences().updateListsEnabled(enabled);
	 }
	
}
