package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import ee.translate.keeleleek.mtapplication.view.dialogs.PullMappingsDialogMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.PullMappingFX;
import ee.translate.keeleleek.mtpluginframework.mapping.PullMapping;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.util.Callback;


public class PullMappingsDialogMediatorFX extends PullMappingsDialogMediator {

	private int total = 1;
	
	@FXML
	private ProgressBar pullProgress;
	@FXML
	private Label actionLabel;
	
	@FXML
	private TableView<PullMappingFX> mappingsTable;
	@FXML
	private TableColumn<PullMappingFX, String> touchedColumn;
	@FXML
	private TableColumn<PullMappingFX, String> groupColumn;
	@FXML
	private TableColumn<PullMappingFX, String> sourceColumn;
	@FXML
	private TableColumn<PullMappingFX, String> destinationColumn;

	@FXML
	private TextField groupEdit;
	@FXML
	private TextField sourceEdit;
	@FXML
	private TextField destinationEdit;

	@FXML
	private CheckBox saveTemplateMappingsCheck;
	
	@FXML
	private HBox progressBox;

	
	private final ObservableList<PullMappingFX> data = FXCollections.observableArrayList();
	
	// INIT
	@FXML
	protected void initialize()
	 {
		touchedColumn.setCellValueFactory(new PropertyValueFactory<PullMappingFX,String>("touched"));

		groupColumn.setCellValueFactory(new PropertyValueFactory<PullMappingFX,String>("group"));
		
		sourceColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PullMappingFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<PullMappingFX, String> mapping) {
				StringProperty prop = new SimpleStringProperty();
				prop.bind(Bindings.concat(mapping.getValue().getSourceProperty()));
				return prop;
			}
		});
		
		destinationColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PullMappingFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<PullMappingFX, String> mapping) {
				StringProperty prop = new SimpleStringProperty();
				prop.bind(mapping.getValue().getDestinationProperty());
				return prop;
			}
		});
		
		mappingsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<PullMappingFX>() {
			@Override
			public void changed(ObservableValue<? extends PullMappingFX> obs, PullMappingFX oldVal, PullMappingFX newVal)
			 {
				if (oldVal != null) {
					oldVal.getGroupProperty().unbind();
					oldVal.getSourceProperty().unbind();
					oldVal.getDestinationProperty().unbind();
				}
				if (newVal != null) {

					groupEdit.setText(newVal.getGroup());
					newVal.getGroupProperty().bind(groupEdit.textProperty());

					sourceEdit.setText(newVal.getSource());
					newVal.getSourceProperty().bind(sourceEdit.textProperty());

					destinationEdit.setText(newVal.getDestination());
					newVal.getDestinationProperty().bind(destinationEdit.textProperty());
					
				}
			 }
		});
	
		
		mappingsTable.setItems(data);
	 }	

	// INHERIT (PROGRESS)
	@Override
	public void setAction(String action) {
		actionLabel.setText(action);
	}
	
	@Override
	public void setCurrent(int current) {
		pullProgress.setProgress((double)current / total);
	}
	
	@Override
	public void setTotal(int total) {
		this.total = total;
	}
	
	@Override
	public void setDone() {
		progressBox.setVisible(false);
	}
	
	@Override
	public boolean isSaveTemplateMappings() {
		return saveTemplateMappingsCheck.isSelected();
	}
	
	
	// INHERIT (ASSOCIATION)
	@Override
	public void addMapping(final PullMapping mapping) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				data.add(new PullMappingFX(mapping));
			}
		});
	}
	
	@Override
	public void applyMappings() {
		for (PullMappingFX associationFX : data) {
			associationFX.apply();
		}
	}
	
	
}
