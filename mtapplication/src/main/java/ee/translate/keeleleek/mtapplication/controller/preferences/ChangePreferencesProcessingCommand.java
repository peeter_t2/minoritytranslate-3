package ee.translate.keeleleek.mtapplication.controller.preferences;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.ProcessingPreferences;

public class ChangePreferencesProcessingCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		ProcessingPreferences preferences = (ProcessingPreferences) notification.getBody();
		
		MinorityTranslateModel.preferences().changeProcessing(preferences);
	 }
	
}
