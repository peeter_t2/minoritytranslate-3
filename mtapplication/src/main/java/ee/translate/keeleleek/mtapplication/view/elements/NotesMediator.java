package ee.translate.keeleleek.mtapplication.view.elements;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.LazyCaller;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;


public abstract class NotesMediator extends Mediator {

	public final static String NAME = "{871DA2CF-5CC0-4168-A898-FD716DD4083E}";
	
	public final static long TEXT_DELAY = 1000;
	
	
	private LazyCaller textCaller;
	
	
	// INIT
	public NotesMediator()
	 {
		super(NAME, null);
	 }

	@Override
	public void onRegister()
	 {
		textCaller = new LazyCaller(new Runnable() {
			@Override
			public void run() {
				MinorityTranslateModel.notifier().sendNotification(Notifications.NOTES_UPDATE, getText());
			}
		}, TEXT_DELAY);
	 }
	

	// NOTIFICATIONS
	@Override
	public String[] listNotificationInterests() {
		return new String[]
		 {
			Notifications.SESSION_LOADED,
		};
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		switch (notification.getName()) {
		case Notifications.SESSION_LOADED:
			setText(MinorityTranslateModel.session().getNotes());
			break;
				
		default:
			break;
		}
	 }
	
	
	
	// INHERIT (CONTENT)
	public abstract String getText();
	
	protected abstract void setText(String text);
	
	
	// EVENTS
	public void onTextEdited() {
		textCaller.call();
	}
	
	
}
