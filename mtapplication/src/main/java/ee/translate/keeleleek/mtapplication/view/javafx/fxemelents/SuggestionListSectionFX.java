package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SuggestionListSectionFX implements SuggestionListFX {

	private final StringProperty name;
	
	
	public SuggestionListSectionFX(String name)
	 {
		this.name = new SimpleStringProperty(name);
	 }

	public StringProperty name() {
		return name;
	}
	
	@Override
	public String getFullName() {
		return name.get();
	}
	
	@Override
	public String toString() {
		return name.get();
	}

	
}
