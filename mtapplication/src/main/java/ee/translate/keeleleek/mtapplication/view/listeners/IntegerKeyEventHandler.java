package ee.translate.keeleleek.mtapplication.view.listeners;

import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

public class IntegerKeyEventHandler implements EventHandler<KeyEvent> {

	@Override
	public void handle(KeyEvent event) {
		if (!event.getCharacter().matches("\\d")) {
			event.consume();
		}
	}

}
