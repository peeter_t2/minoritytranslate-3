package ee.translate.keeleleek.mtapplication.view.pages;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.FindReplaceRequest;


public abstract class FindAddonPageMediator extends Mediator implements AddonsPage {

	public final static String NAME = "{1CCBD06B-E3A6-41A9-906B-8819F4D9DA9D}";
	
	
	// INIT
	public FindAddonPageMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister()
	 {
	 }
	
	public String[] listNotificationInterests() {
		return new String[]
		 {
		 };
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		
	 }

	
	// EVENTS
	public void onFind(FindReplaceRequest request) {
		sendNotification(Notifications.REQUEST_FIND, request);
	}
	
	
}
