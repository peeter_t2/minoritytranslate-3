package ee.translate.keeleleek.mtapplication.controller.preferences;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.InsertsPreferences;

public class ChangePreferencesTemplatesCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		InsertsPreferences preferences = (InsertsPreferences) notification.getBody();
		
		MinorityTranslateModel.preferences().changeInserts(preferences);
	 }
	
}
