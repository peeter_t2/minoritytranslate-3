package ee.translate.keeleleek.mtapplication.view.pages;

import java.util.List;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.LookupRequest.LookupResponse;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;


public abstract class LookupAddonPageMediator extends Mediator implements AddonsPage {

	public final static String NAME = "{11C695F4-1D47-4905-8C9A-C4C19FDC11A0}";
	
	
	// INIT
	public LookupAddonPageMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister()
	 {
		changeLookups(MinorityTranslateModel.preferences().getLookups().getLookupNames());
	 }
	
	public String[] listNotificationInterests() {
		return new String[]
		 {
			Notifications.REQUEST_FULFILLED,
			Notifications.PREFERENCES_LOOKUPS_CHANGED
		};
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		switch (notification.getName()) {

		case Notifications.REQUEST_FULFILLED:
			if (notification.getBody() instanceof LookupResponse) {
				LookupResponse response = (LookupResponse) notification.getBody();
				if (NAME.equals(response.getTarget())) searchFound(response);
			}
			break;

		case Notifications.PREFERENCES_LOOKUPS_CHANGED:
			changeLookups(MinorityTranslateModel.preferences().getLookups().getLookupNames());
			break;
			
		default:
			break;
		}
		
	 }

	
	// INHERIT
	protected abstract void changeLookups(List<String> names);
	protected abstract void searchFound(LookupResponse response);
	
	
	
}
