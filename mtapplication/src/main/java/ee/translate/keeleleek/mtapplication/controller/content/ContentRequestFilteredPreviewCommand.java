package ee.translate.keeleleek.mtapplication.controller.content;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class ContentRequestFilteredPreviewCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		Reference ref = (Reference) notification.getBody();
		
		MinorityTranslateModel.content().requestFilteredPreview(ref);
	 }
	
}
