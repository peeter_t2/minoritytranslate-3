package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.Arrays;

public class SnippetsPreferences {

	private String[] snippets;
	
	
	// INIT
	public SnippetsPreferences()
	 {
		this.snippets = new String[9];
	 }
	

	// PREFERENCES
	public String getSnippet(int i) {
		if (snippets[i] == null) return "";
		return snippets[i];
	}
	
	public void setSnippet(int i, String snippet) {
		this.snippets[i] = snippet;
	}


	// UTILITY
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(snippets);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		SnippetsPreferences other = (SnippetsPreferences) obj;
		if (!Arrays.equals(snippets, other.snippets)) return false;
		return true;
	}
	
	
}
