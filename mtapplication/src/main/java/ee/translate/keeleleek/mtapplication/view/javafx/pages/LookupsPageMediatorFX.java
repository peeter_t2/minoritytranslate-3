package ee.translate.keeleleek.mtapplication.view.javafx.pages;

import java.util.ArrayList;

import ee.translate.keeleleek.mtapplication.model.preferences.Lookup;
import ee.translate.keeleleek.mtapplication.model.preferences.LookupsPreferences;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.LookupFX;
import ee.translate.keeleleek.mtapplication.view.pages.LookupsPageMediator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputControl;
import javafx.util.Callback;


public class LookupsPageMediatorFX extends LookupsPageMediator {

	@FXML
	private TableView<LookupFX> lookupsTable;
	@FXML
	private TableColumn<LookupFX, String> nameColumn;
	@FXML
	private TableColumn<LookupFX, String> urlColumn;

	@FXML
	private Button insertButton;
	@FXML
	private Button appendButton;
	@FXML
	private Button moveUpButton;
	@FXML
	private Button moveDownButton;
	@FXML
	private Button removeButton;

	@FXML
	private TextInputControl nameEdit;
	@FXML
	private TextInputControl urlEdit;
	@FXML
	private TextInputControl parametersEdit;
	@FXML
	private TextInputControl srcElementEdit;
	@FXML
	private TextInputControl dstOpeningEdit;
	@FXML
	private TextInputControl dstElementEdit;
	@FXML
	private TextInputControl dstClosingEdit;
	
	
	// IMPLEMENT
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
	 }

	@FXML
	protected void initialize()
	 {
		// columns
		nameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<LookupFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<LookupFX, String> lookup) {
				return lookup.getValue().name;
			}
		});
		
		urlColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<LookupFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<LookupFX, String> lookup) {
				return lookup.getValue().url;
			}
		});
		
		lookupsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<LookupFX>() {
			@Override
			public void changed(ObservableValue<? extends LookupFX> obs, LookupFX oldVal, LookupFX newVal)
			 {
				if (oldVal != null) {
					oldVal.name.unbind();
					oldVal.url.unbind();
					oldVal.parameters.unbind();
					oldVal.srcElement.unbind();
					oldVal.opening.unbind();
					oldVal.dstElement.unbind();
					oldVal.closing.unbind();
				}
				if (newVal != null) {
					
					nameEdit.setText(newVal.name.getValue());
					newVal.name.bind(nameEdit.textProperty());

					urlEdit.setText(newVal.url.getValue());
					newVal.url.bind(urlEdit.textProperty());

					parametersEdit.setText(newVal.parameters.getValue());
					newVal.parameters.bind(parametersEdit.textProperty());

					srcElementEdit.setText(newVal.srcElement.getValue());
					newVal.srcElement.bind(srcElementEdit.textProperty());

					dstOpeningEdit.setText(newVal.opening.getValue());
					newVal.opening.bind(dstOpeningEdit.textProperty());

					dstElementEdit.setText(newVal.dstElement.getValue());
					newVal.dstElement.bind(dstElementEdit.textProperty());

					dstClosingEdit.setText(newVal.closing.getValue());
					newVal.closing.bind(dstClosingEdit.textProperty());
					
				}
			 }
		});
	
		lookupsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<LookupFX>() {
			@Override
			public void changed(ObservableValue<? extends LookupFX> obs, LookupFX oldVal, LookupFX newVal)
			 {
				appendButton.setDisable(false);
				insertButton.setDisable(false);
				moveUpButton.setDisable(newVal == null);
				moveUpButton.setDisable(newVal == null);
				moveDownButton.setDisable(newVal == null);
				removeButton.setDisable(newVal == null);
				nameEdit.setDisable(newVal == null);
				urlEdit.setDisable(newVal == null);
				parametersEdit.setDisable(newVal == null);
				srcElementEdit.setDisable(newVal == null);
				dstOpeningEdit.setDisable(newVal == null);
				dstElementEdit.setDisable(newVal == null);
				dstClosingEdit.setDisable(newVal == null);
				if (newVal == null) {
					nameEdit.setText("");
					urlEdit.setText("");
					parametersEdit.setText("");
					srcElementEdit.setText("");
					dstOpeningEdit.setText("");
					dstElementEdit.setText("");
					dstClosingEdit.setText("");
				}
			 }
		});
		
	 }

	
	// PAGE
	@Override
	public void open(LookupsPreferences lookups)
	 {
		ObservableList<LookupFX> lookupListFX = FXCollections.observableArrayList();
		for (int i = 0; i < lookups.getLookupCount(); i++) {
			lookupListFX.add(new LookupFX(lookups.getLookup(i)));
		}
		
		lookupsTable.setItems(lookupListFX);
		
		nameEdit.setText("");
		urlEdit.setText("");
		parametersEdit.setText("");
		srcElementEdit.setText("");
		dstOpeningEdit.setText("");
		dstElementEdit.setText("");
		dstClosingEdit.setText("");
		
		lookupsTable.getSelectionModel().selectFirst();
	 }
	
	@Override
	public LookupsPreferences close()
	 {
		ObservableList<LookupFX> lookupListFX = lookupsTable.getItems();
		ArrayList<Lookup> lookupList = new ArrayList<>(lookupListFX.size());
		for (LookupFX lookupFX : lookupListFX) {
			lookupList.add(lookupFX.toLookup());
		}
		
		return new LookupsPreferences(lookupList);
	 }
	
	
	// TABLE
	private void addRow(int index)
	 {
		ObservableList<LookupFX> items = lookupsTable.getItems();
		if (index > items.size()) index = items.size();
		if (index < 0) index = 0;
		
		items.add(index, new LookupFX(new Lookup("", "", "", "", "", "", "")));
		lookupsTable.getSelectionModel().select(items.get(index));
	 }
	
	private void removeRow(int index)
	 {
		ObservableList<LookupFX> items = lookupsTable.getItems();
		if (index >= items.size()) return;
		if (index < 0) return;

		boolean select = lookupsTable.getSelectionModel().getSelectedIndex() != -1;
		
		items.remove(index);
		
		if (select) {
			if (index == items.size()) index--;
			lookupsTable.getSelectionModel().select(index);
		}
	 }

	private void moveRowDown(int index)
	 {
		ObservableList<LookupFX> items = lookupsTable.getItems();
		if (index >= items.size() - 1) return;
		if (index < 0) return;

		boolean select = lookupsTable.getSelectionModel().getSelectedIndex() != -1;
		
		LookupFX item = items.remove(index);
		items.add(index + 1, item);

		if (select) {
			lookupsTable.getSelectionModel().select(item);
		}
	 }

	private void moveRowUp(int index)
	 {
		ObservableList<LookupFX> items = lookupsTable.getItems();
		if (index >= items.size()) return;
		if (index < 1) return;

		boolean select = lookupsTable.getSelectionModel().getSelectedIndex() != -1;
		
		LookupFX item = items.remove(index);
		items.add(index - 1, item);

		if (select) {
			lookupsTable.getSelectionModel().select(item);
		}
	 }
	
	
	// ACTIONS
	@FXML
	private void onAppendClick() {
		addRow(lookupsTable.getSelectionModel().getSelectedIndex() + 1);
	}
	
	@FXML
	private void onInsertClick() {
		addRow(lookupsTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onMoveUpClick() {
		moveRowUp(lookupsTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onMoveDownClick() {
		moveRowDown(lookupsTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onRemoveClick() {
		removeRow(lookupsTable.getSelectionModel().getSelectedIndex());
	}

	
}
