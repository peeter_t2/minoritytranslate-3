package ee.translate.keeleleek.mtapplication.view.javafx.elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.common.requests.FindReplaceRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoice;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoices;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.application.ApplicationMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.editors.EditorMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.helpers.AutocompleteBox;
import ee.translate.keeleleek.mtpluginframework.spellcheck.Misspell;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Popup;
import netscape.javascript.JSException;
import netscape.javascript.JSObject;

public abstract class AceEditWrapper {
	
	// CONSTANTS (KEYS)
	public static KeyCombination AUTOCOMPLETE_KEYS = new KeyCodeCombination(KeyCode.SPACE, KeyCombination.SHORTCUT_DOWN);
	public static KeyCombination PASTE_KEYS = new KeyCodeCombination(KeyCode.V, KeyCombination.SHORTCUT_DOWN);
	
	// CONSTANTS (COMMANDS)
	private static final String CARET_COORDINATES_FUNCTION = "findCaretCoordinates()";
	private static final String CARET_PRECEEDING_FUNCTION = "findPreceding()";
	private static final String CARET_SELECTED_FUNCTION = "findSelected()";
	private static final String CARET_EXTEND_BACKWARD = "extendBackward(#)";
	private static final String CARET_MOVE_BACKWARD = "moveBackward(#)";
	private static final String GET_VALUE = "editor.getValue()";
	private static final String PASTE = "editor.insert(\"#\", true)";
	private static final String SET_READONLY = "editor.setOptions({ readOnly: true } )";
	private static final String SET_EDITABLE = "editor.setOptions({ readOnly: false } )";

	// style
	public static final String EDITOR_STYLESHEET_PATH = EditorMediator.class.getResource("/css/editor.css").toExternalForm();

	// wrap
	private final WebView aceEdit;
	
	// autocomplete
	private Popup popup = new Popup();
	private ListView<AutocompleteBox> popupContent = new ListView<>();
	
	// spelling
	private List<Misspell> misspells = new ArrayList<>();
	private Tooltip misspellTooltip = null;
	
	// loading
	private boolean loaded = false;
	private String loadText = null;
	private List<Misspell> loadMisspells= null;
	
	// control
	private boolean editable;
	private boolean eventLock;
	
	
	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public AceEditWrapper(WebView aceEdit)
	 {
		this.aceEdit = aceEdit;
		
		initialize();
	 }
	
	private void initialize()
	 {
		// keys
		initializeKeys();
		
		// editor
		initializeEditor();
		
		// popup
		initializePopup();
		
		// text
		aceEdit.setContextMenuEnabled(false);
	 }
	
	private void initializeKeys()
	 {
		// block keys
		aceEdit.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				int code = event.getCharacter().codePointAt(0);
				if (popup.isShowing() && code >= 10 && code <= 13) event.consume(); // block new lines
				if (code == 32 && (event.isControlDown() || event.isMetaDown())) event.consume(); // block autocomplete from entering space
			}
		});
		
		// shortcuts
		aceEdit.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e)
			 {
				// autocomplete
				if (AUTOCOMPLETE_KEYS.match(e)) requestAutocomplete();
				if (popup.isShowing() && e.getCode() != KeyCode.UP && e.getCode() != KeyCode.DOWN) requestAutocomplete();
				
				// paste
				if (PASTE_KEYS.match(e)) {
					Clipboard clipboard = Clipboard.getSystemClipboard();
					String content = clipboard.getString();
					if (content != null) aceEdit.getEngine().executeScript(PASTE.replace("#", StringEscapeUtils.escapeEcmaScript(content)));
				}
			 }
		});

		aceEdit.addEventHandler(KeyEvent.ANY, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if (event.isControlDown() && event.isAltDown() && event.isShortcutDown()) {
					event.consume(); // TODO Resolve hack for Windows altgr problem!	
				}
			}
		});
	 }
	
	private void initializeEditor()
	 {
		final AceEditWrapper callback = this;
		
		// when editor finishes loading
		aceEdit.getEngine().getLoadWorker().stateProperty().addListener(
			new ChangeListener<State>() {
				public void changed(ObservableValue<? extends State> ov, State oldState, State newState)
				 {
					if (newState != Worker.State.SUCCEEDED) return;
					
					JSObject jsobj = (JSObject) aceEdit.getEngine().executeScript("window");
					jsobj.setMember("mediator", callback);
					
					queueAfterLoad();
				}
			});

		// load editor
		try {
			String editingTemplate = FileUtil.readFromJar("/editview.html");
			URL jsroot = EditorMediatorFX.class.getResource("/js");
			editingTemplate = editingTemplate.replace("${jsroot}", jsroot.toString());
			editingTemplate = editingTemplate.replace("${FONT_CSS_URL}", getClass().getResource(ApplicationMediatorFX.findFontSizeCSS(MinorityTranslateModel.preferences().getFontSize())).toExternalForm());
			
			eventLock = true;
			
			String text = "";
			text = editingTemplate.replace("${text}", text);
			
			aceEdit.getEngine().loadContent(text);
			eventLock = false;
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	 }
	
	private void initializePopup()
	 {
		popup.getScene().getStylesheets().add(getClass().getResource(ApplicationMediatorFX.findFontSizeCSS(MinorityTranslateModel.preferences().getFontSize())).toExternalForm());
		
		popupContent.setMinWidth(400);
		
		popupContent.addEventFilter(MouseEvent.MOUSE_CLICKED ,new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event)
			 {
				if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
					insertAutocomplete(popupContent.getSelectionModel().getSelectedItem().getChoise());
					event.consume();
					popup.hide();
				}
			 }
		});
		
		popupContent.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event)
			 {
				switch (event.getCode()) {
				case ENTER:
					insertAutocomplete(popupContent.getSelectionModel().getSelectedItem().getChoise());
				case ESCAPE:
					event.consume();
					popup.hide();	
					break;

				default:
					break;
				}
			 }
		});
		
		popup.getContent().addAll(popupContent);
		popup.setAutoHide(true);
	 }
	
	private void queueAfterLoad()
	 {
		if (aceEdit.getEngine().getDocument() != null) { // everything is already loaded
			initializeAfterLoad();
			return;
		}
		
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					while (aceEdit.getEngine().getDocument() == null) {
						Thread.sleep(100L);
					}
				
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							initializeAfterLoad();
						}
					});
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		});
		thread.start(); // wait for the ace scripts to load
	 }
	
	private void initializeAfterLoad()
	 {
		if (loaded) return;
		
		if (aceEdit.getEngine().getDocument() == null) return;		
		
		loaded = true;
		
		// dump text
		if (loadText != null) {
			setText(loadText);
			loadText = null;
		}
		
		// dump misspells
		if (loadMisspells != null) {
			markMisspells(loadMisspells);
			loadMisspells = null;
		}
		
		// style
		aceEdit.getEngine().setUserStyleSheetLocation(EDITOR_STYLESHEET_PATH);
		
		// editable
		setTextEditEditable(editable); // make sure editable is correct after load
	 }
	
	
	
	/* ******************
	 *                  *
	 *      Editor      *
	 *                  *
	 ****************** */
	public Point2D findCaretPosition()
	 {
		String coordinates = aceEdit.getEngine().executeScript(CARET_COORDINATES_FUNCTION).toString();
		String[] strCoords = coordinates.split(",");
		if (strCoords.length < 2) return null;
		Point2D pos = aceEdit.localToScreen(Integer.parseInt(strCoords[0]), Integer.parseInt(strCoords[1]));
		pos = pos.add(0, 24);
		return pos;
	 }
	
	public void setTextEditEditable(boolean editable)
	 {
		this.editable = editable;
		
		if (aceEdit.getEngine().getDocument() != null) {
			if (editable) {
				aceEdit.getEngine().executeScript(SET_EDITABLE);
				aceEdit.getEngine().executeScript("editor.renderer.$cursorLayer.element.style.opacity=0.75");
			}
			else {
				aceEdit.getEngine().executeScript(SET_READONLY);
				aceEdit.getEngine().executeScript("editor.renderer.$cursorLayer.element.style.opacity=0.1");
			}
		}
	 }
	
	public void addStylesheet(Document doc, String cssLocation)
	 {
		// http://stackoverflow.com/questions/27604232/javafx-webview-add-stylesheet-parsing-calc-error
		Element docElement = doc.getDocumentElement();
	    NodeList heads = docElement.getElementsByTagName("head");
	    Element head ;
	    if (heads.getLength() == 0) {
	        head = doc.createElement("head");
	        docElement.appendChild(head);
	    } else {
	        head = (Element) heads.item(0);
	    }
	    Element link = doc.createElement("link");
	    link.setAttribute("rel", "stylesheet");
	    link.setAttribute("type", "text/css");
	    link.setAttribute("href", cssLocation);
	    head.appendChild(link);     
	 }
	
	

	/* ******************
	 *                  *
	 *   Autocomplete   *
	 *                  *
	 ****************** */
	private void requestAutocomplete()
	 {
		String preceeding;
		String selected;
		
		try {
			
			preceeding = aceEdit.getEngine().executeScript(CARET_PRECEEDING_FUNCTION).toString();
			selected = aceEdit.getEngine().executeScript(CARET_SELECTED_FUNCTION).toString();
			
		} catch (JSException e) {
			e.printStackTrace();
			return;
		}
		
		onUserRequestAutocomplete(preceeding, selected);
	}
	
	public void showAutocomplete(AutocompleteChoices autocomplete)
	 {
		// create list
		popupContent.getItems().clear();
		popupContent.setPrefHeight(200);
		List<AutocompleteChoice> choises = autocomplete.getChoises();
		
		// insert automatically
		if (MinorityTranslateModel.preferences().isInsertAuto() && choises.size() == 1 && !popup.isShowing()) {
			insertAutocomplete(choises.get(0));
			return;
		}
		
		for (AutocompleteChoice choise : choises) {
			popupContent.getItems().add(new AutocompleteBox(choise));
		}
		
		//hide
		if (popupContent.getItems().size() == 0) {
			popup.hide();
			return;
		}

		// show
		if (!popup.isShowing()) {
			Point2D pos = findCaretPosition();
			if (pos == null) return;
			popup.show(aceEdit, pos.getX(), pos.getY());
			popup.requestFocus();
		}
	 }

	public void insertAutocomplete(AutocompleteChoice choice)
	 {
		if (!editable) return;
		int textOffset = choice.getTextBackset();
		if (textOffset != 0) aceEdit.getEngine().executeScript(CARET_EXTEND_BACKWARD.replace("#", textOffset + ""));
		pasteText(choice.getText());
		int caretOffset = choice.getCaretBackset();
		if (caretOffset != 0) aceEdit.getEngine().executeScript(CARET_MOVE_BACKWARD.replace("#", caretOffset + ""));
	 }
	
	
	
	/* ******************
	 *                  *
	 *       Text       *
	 *                  *
	 ****************** */
	public String getText()
	 {
		return aceEdit.getEngine().executeScript(GET_VALUE).toString();
	 }

	public void setText(String text)
	 {
		if (!loaded) { // wait for the editor to finish loading
			loadText = text;
			return;
		}
		
		if (aceEdit.getEngine().getDocument() == null) return;
		
		eventLock = true;
		
		text = StringEscapeUtils.escapeEcmaScript(text);
		
		aceEdit.getEngine().executeScript("editor.session.setValue(\"" + text + "\", -1)");
		
		eventLock = false;
	 }
	
	public void replaceText(String text)
	 {
		if (aceEdit.getEngine().getDocument() == null) return;
		
		text = StringEscapeUtils.escapeEcmaScript(text);
		aceEdit.getEngine().executeScript("editor.setValue(\"" + text + "\", -1)");
	 }

	public void pasteText(String text)
	 {
		if (!aceEdit.isFocused()) return;

		text = StringEscapeUtils.escapeEcmaScript(text);
		aceEdit.getEngine().executeScript("editor.insert(\"" + text + "\", -1)");
	 }
	
	
	
	/* ******************
	 *                  *
	 *     Spelling     *
	 *                  *
	 ****************** */
	public void markMisspells(List<Misspell> misspells)
	 {
		if (!loaded) {
			loadMisspells = misspells;
			return;
		}
		
		this.misspells = misspells;
		
		aceEdit.getEngine().executeScript("clearMisspells()");
		for (Misspell misspell : misspells) {
			aceEdit.getEngine().executeScript("markMisspell(" + misspell.getStartRow() + "," + misspell.getStartColumn() + "," + misspell.getEndRow() + "," + misspell.getEndColumn() + ")");
		}
	 }
	
	public void showMisspellMessage(Misspell misspell)
	 {
		if (!misspell.hasMessage()) return;
		
		// find position
		Point2D pos = findCaretPosition();
		if (pos == null) return;

		// show tooltip
		misspellTooltip = new Tooltip(misspell.getMessage());
		misspellTooltip.show(aceEdit, pos.getX(), pos.getY());
	 }
	
	public void closeMisspellMessage()
	 {
		if (misspellTooltip == null) return;
		
		misspellTooltip.hide();
		misspellTooltip = null;
	 }
		
	public Misspell findMisspell(int row, int col)
	 {
		for (Misspell misspell : misspells) {
			if (misspell.getStartRow() <= row && row <= misspell.getEndRow())
				if (misspell.getStartColumn() <= col && col <= misspell.getEndColumn()) return misspell;
		}
		return null;
	 }

	
	
	/* ******************
	 *                  *
	 *   Find/Replace   *
	 *                  *
	 ****************** */
	public void findReplace(FindReplaceRequest request)
	 {
		String find = "'" + StringEscapeUtils.escapeEcmaScript(request.getFind()) + "'";
		String replace = "'" + StringEscapeUtils.escapeEcmaScript(request.getReplace()) + "'";
		String options;
		
		switch (request.getType()) {
		case FIND_NEXT:
			options = "" + false + "," + request.isWrap() + "," + request.isCaseSensitive() + "," + request.isWholeWord();
			aceEdit.getEngine().executeScript("find("+ find + "," + options + ")");
			break;
			
		case FIND_PREVIOUS:
			options = "" + true + "," + request.isWrap() + "," + request.isCaseSensitive() + "," + request.isWholeWord();
			aceEdit.getEngine().executeScript("find("+ find + "," + options + ")");
			break;

		case REPLACE:
			aceEdit.getEngine().executeScript("replaceFind("+ replace + ")");
			options = "" + false + "," + request.isWrap() + "," + request.isCaseSensitive() + "," + request.isWholeWord();
			aceEdit.getEngine().executeScript("find("+ find + "," + options + ")");
			break;

		case REPLACE_ALL:
			options = "" + false + "," + request.isWrap() + "," + request.isCaseSensitive() + "," + request.isWholeWord();
			aceEdit.getEngine().executeScript("replaceAll("+ find + "," + replace + "," + options + ")");
			break;

		default:
			break;
		}
	 }
	
	
	
	/* ******************
	 *                  *
	 *   Ace callback   *
	 *                  *
	 ****************** */
	public void onAceEdit()
	 {
		if (eventLock) return;
		onUserEditText();
	 }

	public void onAceScroll()
	 {
		closeMisspellMessage();
	 }
  
	public void onAceCaretMove(String row, String column)
	 {
		closeMisspellMessage();
  	
		int line = Integer.parseInt(row);
		int col = Integer.parseInt(column);
  	
		Misspell misspell = findMisspell(line, col);
		if (misspell != null) showMisspellMessage(misspell);
	 }

	public void onAceCopy(String text)
	 {
		if (text.isEmpty()) return;
		Clipboard clipboard = Clipboard.getSystemClipboard();
		ClipboardContent content = new ClipboardContent();
		content.putString(text);
		clipboard.setContent(content);
	 }

	public void onAcePaste(Object object)
	 {
	 }

	
	
	/* ******************
	 *                  *
	 * Callback events  *
	 *                  *
	 ****************** */
	public abstract void onUserRequestAutocomplete(String preceedingText, String selectedText);
	
	public abstract void onUserEditText();
	
	
}
