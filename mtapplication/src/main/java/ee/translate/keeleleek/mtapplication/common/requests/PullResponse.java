package ee.translate.keeleleek.mtapplication.common.requests;

import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class PullResponse {

	private Reference dstRef;
	private String text;
	
	
	public PullResponse(Reference dstRef, String text) {
		super();
		this.dstRef = dstRef;
		this.text = text;
	}

	
	public Reference getDstRef() {
		return dstRef;
	}
	
	public String getText() {
		return text;
	}
	
	
}
